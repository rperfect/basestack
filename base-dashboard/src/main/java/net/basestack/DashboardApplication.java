package net.basestack;

import net.basestack.config.MyWebMvcConfigurerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.IOException;


@Configuration
@ComponentScan("net.basestack")
@EnableAutoConfiguration
public class DashboardApplication {

    @Bean
    public WebMvcConfigurerAdapter webMvcConfigurerAdapter(@Value("${version}")String version) {
        WebMvcConfigurerAdapter webMvcConfigurerAdapter = new MyWebMvcConfigurerAdapter(version);
        return webMvcConfigurerAdapter;
    }



    public static void main(String[] args) throws IOException {
        SpringApplication.run(DashboardApplication.class, args);
    }
}
