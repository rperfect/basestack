package net.basestack.services.EnvironmentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 */
@RestController
@RequestMapping("/rest/environments")
public class EnvironmentController {

    @Autowired
    protected EnvironmentService environmentService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<EnvironmentData> list() {
        return environmentService.getEnvironmentData();
    }

}
