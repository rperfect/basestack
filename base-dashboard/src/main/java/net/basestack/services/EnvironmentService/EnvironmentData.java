package net.basestack.services.EnvironmentService;

import java.util.List;

/**
 */
public class EnvironmentData {

    private String name;
    private String liveURL;
    private String stageURL;

    private List<StackStatusSummary> elbStatusList;
    private List<StackStatusSummary> stageStatusList;
    private List<StackStatusSummary> liveStatusList;
    private List<StackStatusSummary> databaseStatusList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLiveURL() {
        return liveURL;
    }

    public void setLiveURL(String liveURL) {
        this.liveURL = liveURL;
    }

    public String getStageURL() {
        return stageURL;
    }

    public void setStageURL(String stageURL) {
        this.stageURL = stageURL;
    }

    public List<StackStatusSummary> getElbStatusList() {
        return elbStatusList;
    }

    public void setElbStatusList(List<StackStatusSummary> elbStatusList) {
        this.elbStatusList = elbStatusList;
    }

    public List<StackStatusSummary> getStageStatusList() {
        return stageStatusList;
    }

    public void setStageStatusList(List<StackStatusSummary> stageStatusList) {
        this.stageStatusList = stageStatusList;
    }

    public List<StackStatusSummary> getLiveStatusList() {
        return liveStatusList;
    }

    public void setLiveStatusList(List<StackStatusSummary> liveStatusList) {
        this.liveStatusList = liveStatusList;
    }

    public List<StackStatusSummary> getDatabaseStatusList() {
        return databaseStatusList;
    }

    public void setDatabaseStatusList(List<StackStatusSummary> databaseStatusList) {
        this.databaseStatusList = databaseStatusList;
    }
}
