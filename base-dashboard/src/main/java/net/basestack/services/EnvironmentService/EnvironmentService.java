package net.basestack.services.EnvironmentService;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudformation.AmazonCloudFormation;
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient;
import com.amazonaws.services.cloudformation.model.DescribeStacksResult;
import com.amazonaws.services.cloudformation.model.Output;
import com.amazonaws.services.cloudformation.model.Stack;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 */
@Component
public class EnvironmentService {

    public List<EnvironmentData> getEnvironmentData() {
        List<EnvironmentData> environmentList = new ArrayList<>();
        List<String> nameList = getEnvironmentNames();
        for(String nextName : nameList) {
            environmentList.add(getEnvironmentData(nextName));
        }

        return environmentList;
    }

    protected List<String> getEnvironmentNames() {
        List<String> nameList = new ArrayList<>();
        File[] files = getEnvironmentHomeDir().listFiles();
        for(File nextFile : files) {
            if(nextFile.isDirectory() && !nextFile.getName().equals("defaults")) {
                nameList.add(nextFile.getName());
            }
        }
        return nameList;
    }

    protected File getEnvironmentHomeDir() {
        return new File("C:\\Projects\\BaseStack\\base-ant\\environments");
    }

    protected EnvironmentData getEnvironmentData(String name) {

        Properties properties = getEnvironmentProperties(name);


        List<Stack> stackList = describeStacks();

        String elbStackName   = name + "-stack-elb";
        String stageStackName = name + "-stack-app-stage";
        String liveStackName  = name + "-stack-app-live";

        List<StackStatusSummary> elbStatusList = getStackStatus(elbStackName, stackList);
        List<StackStatusSummary> stageStatusList = getStackStatus(stageStackName, stackList);
        List<StackStatusSummary> liveStatusList = getStackStatus(liveStackName, stackList);

        EnvironmentData environmentData = new EnvironmentData();
        environmentData.setName(name);
        environmentData.setStageURL(getApplicationURL("dns.subDomain.stage", properties));
        environmentData.setLiveURL(getApplicationURL("dns.subDomain.live", properties));
        environmentData.setElbStatusList(elbStatusList);
        environmentData.setStageStatusList(stageStatusList);
        environmentData.setLiveStatusList(liveStatusList);

        return environmentData;
    }

    private String getApplicationURL(String subdomainPropertyKey, Properties properties) {
        String domainName = "base-stack.net";
        String applicationURL = "http://" + properties.getProperty(subdomainPropertyKey) + "." + domainName + "/";
        return applicationURL;
    }

    private Properties getEnvironmentProperties(String name) {
        try {
            Properties properties = new Properties();
            File propertiesFile = new File(getEnvironmentHomeDir() + File.separator + name, "environment.properties");
            if(propertiesFile.exists()) {
                properties.load(new BufferedInputStream(new FileInputStream(propertiesFile)));
            }
            return properties;
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private List<Stack> describeStacks() {
        AmazonCloudFormation amazonCloudFormationClient = new AmazonCloudFormationClient();
        amazonCloudFormationClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_2));
        DescribeStacksResult result = amazonCloudFormationClient.describeStacks();
        return result.getStacks();
    }

    private List<StackStatusSummary> getStackStatus(String stackName, List<Stack> stackList) {
        List<StackStatusSummary> summaryList = new ArrayList<>();
        for(Stack nextStack : stackList) {
            if(nextStack.getStackName().startsWith(stackName)) {
                StackStatusSummary summary = new StackStatusSummary();
                summary.setName(nextStack.getStackName());
                summary.setStatus(nextStack.getStackStatus());
                summary.setStatusReason(nextStack.getStackStatusReason());
                summary.setVersion(getVersion(nextStack));
                summaryList.add(summary);
            }
        }

        if(summaryList.size() == 0) {
            StackStatusSummary summary = new StackStatusSummary();
            summary.setName(stackName);
            summary.setStatus("NOT_FOUND");
            summaryList.add(summary);
        }

        return summaryList;
    }

    private String getVersion(Stack stack) {
        String version = getOutputsMap(stack).get("WebApplicationVersion");
        return version;
    }

    private Map<String, String> getOutputsMap(Stack stack) {
        Map<String, String> outputsMap = new HashMap<>();
        for(Output nextOutput : stack.getOutputs()) {
            outputsMap.put(nextOutput.getOutputKey(), nextOutput.getOutputValue());
        }
        return outputsMap;
    }

}
