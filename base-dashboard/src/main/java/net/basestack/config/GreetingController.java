package net.basestack.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GreetingController {

    @Value("${version}")
    protected String version;

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("version", version);
        return "index";
    }

}