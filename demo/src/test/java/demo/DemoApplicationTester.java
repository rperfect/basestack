package demo;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DemoApplicationTester {

    public DemoApplicationTester() {
        FileAppender<ILoggingEvent> myAppender = new FileAppender<ILoggingEvent>();
        LoggerContext loggerContext=(LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.reset();

        PatternLayoutEncoder layout=new PatternLayoutEncoder();
        layout.setContext(loggerContext);
        layout.setPattern("%X{first} %X{last} - %m%n");
        layout.start();
        myAppender.setAppend(true);
        myAppender.setFile("logs/testFile1.log");
        myAppender.setName("File1");
        myAppender.setEncoder(layout);
        myAppender.setContext(loggerContext);
        myAppender.start();
        ch.qos.logback.classic.Logger logbackLogger = loggerContext.getLogger("Main");
        logbackLogger.addAppender(myAppender);
        logbackLogger.setLevel(Level.OFF);
        logbackLogger.setAdditive(true);
    }

	public void ping() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        String previousContentBody = null;
        while(true) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> content = restTemplate.getForEntity("http://dev.base-stack.net/", String.class);

                String time = dateFormat.format(new Date());
                String contentBody = content.getBody();

                String message;
                if(previousContentBody != null && !previousContentBody.equals(contentBody)) {
                    message = "CHANGED - " + contentBody;
                }
                else {
                    message = contentBody;
                }
                previousContentBody = contentBody;

                System.out.println(time + ": " + message);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException ex) {

            }
        }
    }

    public static void main(String[] args) {
        DemoApplicationTester tester = new DemoApplicationTester();
        tester.ping();
    }

}
