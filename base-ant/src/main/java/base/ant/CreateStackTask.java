package base.ant;

import com.amazonaws.services.cloudformation.model.CreateStackRequest;
import com.amazonaws.services.cloudformation.model.Parameter;
import org.apache.commons.io.FileUtils;
import org.apache.tools.ant.BuildException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 */
public class CreateStackTask extends AbstractAWSTask {

    private String template;
    private Integer timeoutInMinutes = 10;
    private Boolean waitforcreation = true;
    private Boolean disableRollback = false;

    private List<StackParameter> stackParameters;

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Integer getTimeoutInMinutes() {
        return timeoutInMinutes;
    }

    public void setTimeoutInMinutes(Integer timeoutInMinutes) {
        this.timeoutInMinutes = timeoutInMinutes;
    }

    public Boolean getWaitforcreation() {
        return waitforcreation;
    }

    public void setWaitforcreation(Boolean waitforcreation) {
        this.waitforcreation = waitforcreation;
    }

    public Boolean getDisableRollback() {
        return disableRollback;
    }

    public void setDisableRollback(Boolean disableRollback) {
        this.disableRollback = disableRollback;
    }

    public List<StackParameter> getStackParameters() {
        if (stackParameters == null) {
            stackParameters = new ArrayList<StackParameter>();
        }
        return stackParameters;
    }

    public StackParameter createStackParameter() {
        StackParameter stackParameter = new StackParameter();
        getStackParameters().add(stackParameter);
        return stackParameter;
    }

    public String getTemplateBody() {
        try {
            String templateBody = FileUtils.readFileToString(new File(getTemplate()));
            return templateBody;
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void execute() throws BuildException {

        // stackName="${stackName}" timeoutInMinutes="15" templatebody="${stackJson}" waitforcreation="true" disableRollback="true"

        CreateStackRequest request = new CreateStackRequest();
        request.setStackName(getStackName());
        request.setTemplateBody(getTemplateBody());
        request.setTimeoutInMinutes(getTimeoutInMinutes());
        request.setDisableRollback(getDisableRollback());

        for(StackParameter nextStackParameter : getStackParameters()) {
            request.getParameters().add(new Parameter().withParameterKey(nextStackParameter.getKey()).withParameterValue(nextStackParameter.getValue()));
        }

        System.out.println("Create stack: " + request.getStackName());
        getCloudFormationClient().createStack(request);

        if(getWaitforcreation()) {
            waitForStatus(getCloudFormationClient(), getStackName(), "CREATE_COMPLETE");
        }
    }
}
