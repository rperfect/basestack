package base.ant;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.cloudformation.model.DeleteStackRequest;
import org.apache.tools.ant.BuildException;

/**
 */
public class DeleteStacksTask extends AbstractAWSTask {

    private String deleteStacksList;

    public String getDeleteStacksList() {
        return deleteStacksList;
    }

    public void setDeleteStacksList(String deleteStacksList) {
        this.deleteStacksList = deleteStacksList;
    }

    @Override
    public void execute() throws BuildException {

        String[] stackNames = deleteStacksList.split(",");

        if(stackNames != null && stackNames.length > 0) {
            for (String nextStackName : stackNames) {
                if(nextStackName != null && nextStackName.trim().length() > 0) {
                    System.out.println("Deleting stack: " + nextStackName);
                    getCloudFormationClient().deleteStack(new DeleteStackRequest().withStackName(nextStackName));

                    try {
                        waitForStatus(getCloudFormationClient(), nextStackName, "DELETE_COMPLETE");
                    }
                    catch (AmazonServiceException ex) {
                        if(ex.getMessage().contains("does not exist")) {
                            // ignore and assume stack is now deleted
                        }
                        else {
                            throw ex;
                        }
                    }
                }
            }
        }
    }
}
