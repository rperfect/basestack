package base.ant;

/**
 */
public class StackParameter {

    private String key;
    private String value;

    public StackParameter() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
