package base.ant;

import com.amazonaws.services.cloudformation.model.Stack;
import org.apache.tools.ant.BuildException;

/**
 */
public class GetLoadBalancerName extends AbstractAWSTask {

    protected String loadBalancerProperty;


    public void setLoadBalancerProperty(String loadBalancerProperty) {
        this.loadBalancerProperty = loadBalancerProperty;
    }

    @Override
    public void execute() throws BuildException {

        String elbStackName = getEnvironment() + "-stack-" + "elb";

        Stack elbStack = describeStack(elbStackName);
        String propertyKey = "ElasticLoadBalancer" + getStackType();
        String loadBalancerName = getOutputValue(propertyKey, elbStack);

        getProject().setProperty(loadBalancerProperty, loadBalancerName);
    }

}
