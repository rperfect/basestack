package base.ant;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.cloudformation.AmazonCloudFormation;
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient;
import com.amazonaws.services.cloudformation.model.DescribeStacksRequest;
import com.amazonaws.services.cloudformation.model.DescribeStacksResult;
import com.amazonaws.services.cloudformation.model.Output;
import com.amazonaws.services.cloudformation.model.Stack;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient;
import org.apache.tools.ant.Task;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class AbstractAWSTask extends Task {


    private String environment;
    private String stackLayer;
    private String stackType;
    private String version;

    private AmazonCloudFormation cloudFormationClient;
    private AmazonElasticLoadBalancing elbClient;
    private AmazonAutoScaling autoScalingClient;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getStackLayer() {
        return stackLayer;
    }

    public void setStackLayer(String stackLayer) {
        this.stackLayer = stackLayer;
    }

    public String getStackType() {
        return stackType;
    }

    public void setStackType(String stackType) {
        this.stackType = stackType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStackVersion() {
        String stackVersion = null;
        if(getVersion() != null) {
            stackVersion = getVersion().replace('.', '-');
        }
        return stackVersion;
    }

    public AmazonCloudFormation getCloudFormationClient() {
        if (cloudFormationClient == null) {
            cloudFormationClient = new AmazonCloudFormationClient();
            cloudFormationClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_2));
        }
        return cloudFormationClient;
    }

    public AmazonElasticLoadBalancing getElbClient() {
        if (elbClient == null) {
            elbClient = new AmazonElasticLoadBalancingClient();
            elbClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_2));
        }
        return elbClient;
    }

    public AmazonAutoScaling getAutoScalingClient() {
        if (autoScalingClient == null) {
            autoScalingClient = new AmazonAutoScalingClient();
            autoScalingClient.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_2));
        }
        return autoScalingClient;
    }

    protected String getStackNameWithoutVersion() {
        List<String> tokenList = new ArrayList<>();
        appendNameToken(tokenList, getEnvironment());
        appendNameToken(tokenList, "stack");
        appendNameToken(tokenList, getStackLayer());
        appendNameToken(tokenList, getStackType());
        return appendNameToken(tokenList);
    }

    protected String getStackName() {
        List<String> tokenList = new ArrayList<>();
        appendNameToken(tokenList, getEnvironment());
        appendNameToken(tokenList, "stack");
        appendNameToken(tokenList, getStackLayer());
        appendNameToken(tokenList, getStackType());
        appendNameToken(tokenList, getStackVersion());
        return appendNameToken(tokenList);
    }

    private void appendNameToken(List<String> tokenList, String token) {
        if(token != null && token.trim().length() > 0) {
            tokenList.add(token);
        }
    }

    private String appendNameToken(List<String> tokenList) {
        StringBuilder bob = new StringBuilder();
        for(int i = 0; i < tokenList.size(); i++) {
            bob.append(tokenList.get(i));
            if(i < tokenList.size() - 1) {
                bob.append("-");
            }
        }
        return bob.toString();
    }


    public static String getOutputValue(String outputKey, Stack stack) {
        String outputValue = null;

        List<Output> outputs = stack.getOutputs();
        for(Output nextOutput : outputs) {
            if(nextOutput.getOutputKey().equals(outputKey)) {
                outputValue = nextOutput.getOutputValue();
                break;
            }
        }

        if(outputValue == null) {
            throw new RuntimeException("Unable to find output value for " + outputKey + " in stack outputs " + stack);
        }

        return outputValue;
    }

    protected Stack describeStack(String stackName) {
        DescribeStacksResult stacksResult = getCloudFormationClient().describeStacks(new DescribeStacksRequest().withStackName(stackName));

        Stack stack;
        List<Stack> stackList = stacksResult.getStacks();
        if(stackList.size() == 0) {
            throw new RuntimeException("Unable to find stack for " + stackName);
        }
        else if(stackList.size() == 1) {
            stack = stacksResult.getStacks().get(0);
        }
        else {
            throw new RuntimeException("Found more than one stack for " + stackName);
        }

        return stack;
    }

    protected static void waitForStatus(AmazonCloudFormation client, String stackName, String status) {
        final int MAX_POLLING = 20;
        final long POLLING_PERIOD_MS = 30 * 1000;

        DescribeStacksRequest describeStacksRequest = new DescribeStacksRequest();
        describeStacksRequest.setStackName(stackName);
        boolean statusReached = false;
        for(int i = 0; i < MAX_POLLING && !statusReached; i++) {
            DescribeStacksResult describeStacksResult = client.describeStacks(describeStacksRequest);

            List<Stack> stacks = describeStacksResult.getStacks();
            if (stacks.size() > 1) {
                throw new RuntimeException("More than one stack found.");
            }

            String stackStatus = stacks.get(0).getStackStatus();
            System.out.println("Stack: " + stackName + " is in status: " + stackStatus);
            if(stackStatus.equals(status)) {
                statusReached = true;
            }
            else {
                try {
                    Thread.sleep(POLLING_PERIOD_MS);
                } catch (InterruptedException ex) {
                }
            }
        }

        if(!statusReached) {
            throw new RuntimeException("Status of " + status + " was not reached within the number of permitted polling attempts");
        }
    }

}
