package base.ant;

import com.amazonaws.services.cloudformation.model.DeleteStackRequest;
import com.amazonaws.services.cloudformation.model.DescribeStacksResult;
import com.amazonaws.services.cloudformation.model.Stack;
import org.apache.tools.ant.BuildException;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class PrepareStacksTask extends AbstractAWSTask {

    private String deleteStacksLaterList;

    public String getDeleteStacksLaterList() {
        return deleteStacksLaterList;
    }

    public void setDeleteStacksLaterList(String deleteStacksLaterList) {
        this.deleteStacksLaterList = deleteStacksLaterList;
    }

    @Override
    public void execute() throws BuildException {

        DescribeStacksResult stacksResult = getCloudFormationClient().describeStacks();
        List<Stack> stackList = stacksResult.getStacks();

        String baseName = getStackNameWithoutVersion();
        List<Stack> deleteLaterList = new ArrayList<>();
        for(Stack nextStack : stackList) {
            if(nextStack.getStackName().startsWith(baseName)) {
                if(nextStack.getStackName().equals(getStackName())) {
                    deleteStack(nextStack);
                }
                else {
                    deleteLaterList.add(nextStack);
                }
            }
        }

        StringBuilder bob = new StringBuilder();
        for(int i = 0; i < deleteLaterList.size(); i++) {
            Stack nextStack = deleteLaterList.get(i);
            bob.append(nextStack.getStackName());
            if(i < deleteLaterList.size() - 1) {
                bob.append(",");
            }
        }

        getProject().setProperty(getDeleteStacksLaterList(), bob.toString());
    }

    private void deleteStack(Stack nextStack) {
        getCloudFormationClient().deleteStack(new DeleteStackRequest().withStackName(nextStack.getStackName()));
        waitForStatus(getCloudFormationClient(), nextStack.getStackName(), "DELETE_COMPLETE");
    }
}
