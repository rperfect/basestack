package base.ant;

import com.amazonaws.services.cloudformation.model.Stack;
import org.apache.tools.ant.BuildException;

/**
 */
public class GetAutoScalingGroupName extends AbstractAWSTask {

    protected String autoScalingGroupProperty;

    public String getAutoScalingGroupProperty() {
        return autoScalingGroupProperty;
    }

    public void setAutoScalingGroupProperty(String autoScalingGroupProperty) {
        this.autoScalingGroupProperty = autoScalingGroupProperty;
    }

    @Override
    public void execute() throws BuildException {

        Stack stack = describeStack(getStackName());
        String propertyKey = "WebServerGroupName";
        String autoScalingGroupName = getOutputValue(propertyKey, stack);

        getProject().setProperty(autoScalingGroupProperty, autoScalingGroupName);

    }
}
