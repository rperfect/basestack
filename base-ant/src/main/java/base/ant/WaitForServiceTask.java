package base.ant;

import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.autoscaling.model.Instance;
import com.amazonaws.services.elasticloadbalancing.model.DescribeInstanceHealthRequest;
import com.amazonaws.services.elasticloadbalancing.model.DescribeInstanceHealthResult;
import com.amazonaws.services.elasticloadbalancing.model.InstanceState;
import org.apache.tools.ant.BuildException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 */
public class WaitForServiceTask  extends AbstractAWSTask {

    protected String loadBalancerName;
    protected String autoScalingGroupName;

    public String getLoadBalancerName() {
        return loadBalancerName;
    }

    public void setLoadBalancerName(String loadBalancerName) {
        this.loadBalancerName = loadBalancerName;
    }

    public String getAutoScalingGroupName() {
        return autoScalingGroupName;
    }

    public void setAutoScalingGroupName(String autoScalingGroupName) {
        this.autoScalingGroupName = autoScalingGroupName;
    }

    @Override
    public void execute() throws BuildException {

        // get the AutoScaling group
        DescribeAutoScalingGroupsResult autoScalingGroupsResult = getAutoScalingClient().describeAutoScalingGroups(new DescribeAutoScalingGroupsRequest().withAutoScalingGroupNames(getAutoScalingGroupName()));
        List<AutoScalingGroup> groups = autoScalingGroupsResult.getAutoScalingGroups();

        if(groups.size() != 1) {
            throw  new RuntimeException("Did not find exactly one auto scaling group with the name " + getAutoScalingGroupName());
        }

        AutoScalingGroup autoScalingGroup = groups.get(0);
        List<Instance> instanceList = autoScalingGroup.getInstances();

        // while not finished
        boolean finished = false;
        while(!finished) {
            // get the load-balancer
            // see if the ASG instance within the ELB as in-service

            DescribeInstanceHealthRequest instanceHealthRequest = new DescribeInstanceHealthRequest(getLoadBalancerName());
            DescribeInstanceHealthResult instanceHealthResult = getElbClient().describeInstanceHealth(instanceHealthRequest);
            List<InstanceState> instanceStates = instanceHealthResult.getInstanceStates();

            Set<String> waitingForInstancesSet = createSet(instanceList);

            for(InstanceState nextInstanceState : instanceStates) {
                if(waitingForInstancesSet.contains(nextInstanceState.getInstanceId()) && nextInstanceState.getState().equals("InService")) {
                    waitingForInstancesSet.remove(nextInstanceState.getInstanceId());
                }
            }

            System.out.println("Waiting for service of " + getAutoScalingGroupName() + " in ELB " + getLoadBalancerName() + " waiting on " + waitingForInstancesSet.size() + " instances out of service.");

            finished = waitingForInstancesSet.size() == 0;
            if(!finished) {
                try {
                    Thread.sleep(20 * 1000);
                }
                catch (InterruptedException ex) {

                }
            }
        }


    }

    private Set<String> createSet(List<Instance> instanceList) {
        Set<String> set = new HashSet<>();
        for(Instance nextInstance : instanceList) {
            set.add(nextInstance.getInstanceId());
        }
        return set;
    }

    public static void main(String[] args) {
        WaitForServiceTask waitForServiceTask = new WaitForServiceTask();
        waitForServiceTask.setLoadBalancerName("dev-stack-ElasticL-J7Q5Z61HSIHJ");
        waitForServiceTask.setAutoScalingGroupName("dev-stack-app-live-1-0-WebServerGroup-13YEY7L7NOPKK");
        waitForServiceTask.execute();
    }
}
