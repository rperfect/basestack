
Tomcat Install
---------------

When creating the image remember to do the following

Install into C:\Java\Tomcat8

1. Change the Windows Service to automatically start
2. Add "manager-script" role to enable Ant deployment
3. Add antiResourceLocking="true" to the context to avoid file locking
4. Add Windows Firewall Settings
5. Add the following to conf/server.xml just before the last </Host> tag (replace app_name)

<Context path="" docBase="app_name">
    <!-- Default set of monitored resources -->
    <WatchedResource>WEB-INF/web.xml</WatchedResource>
</Context>